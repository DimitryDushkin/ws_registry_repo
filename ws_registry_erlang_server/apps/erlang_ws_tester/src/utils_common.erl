%% Author: dimitry
%% Created: 11.06.2011
%% Description: TODO: Add description to utils_common
-module(utils_common).

%%
%% Include files
%%

%%
%% Exported Functions
%%
-export([
         get_timestamp/0,
         pmap/2,
         list_find/2,
         proplist_find/3,
         run/1,
         run/2
        ]).

%%
%% API Functions
%%

%%--------------------------------------------------------------------
%% Return UNIX timestamp in ms
%%--------------------------------------------------------------------
-spec get_timestamp() -> integer().
get_timestamp() ->
	{Mega, Sec, Micro} = os:timestamp(),		%should be faster
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).


%%--------------------------------------------------------------------
%% @spec pmap(F, L) -> list(any())
%%
%% @doc A parallelized version of lists:map/2, but each call to F is
%% executed in its own spawned process.
%%--------------------------------------------------------------------
pmap(F, L) ->
    S = self(),
    Pids = lists:map(fun(I) ->
                         spawn(fun() -> do_f(S, F, I) end)
                     end, L),
    gather(Pids).

gather([H|T]) ->
    receive
        {H, Ret} -> [Ret|gather(T)]
    end;
gather([]) ->
    [].

do_f(Parent, F, I) ->
    Parent ! {self(), (catch F(I))}.


%% ================ Check if such value in list ===========
-spec list_find(string(), list()) -> boolean().
list_find ( _, [] ) ->
    false;

list_find ( Element, [ Item | ListTail ] ) ->
    case ( Item == Element ) of
        true    ->  true;
        false   ->  list_find(Element, ListTail)
    end.

%% ================ Check if such value in proplist ===========
-spec proplist_find(string() | atom(), string() | atom(), list()) -> boolean().
proplist_find (_, _, []) ->
    false;

proplist_find (Key, Value, [ Item | ListTail ]) ->
    case ( proplists:get_value(Key, Item) == Value  ) of
        true    ->  true;
        false   ->  proplist_find(Key, Value, ListTail)
    end.

%% =========================== Run CMD command ================
run(Cmd) -> 
    run(Cmd, 5000).
    
run(Cmd, Timeout) ->
    Port = erlang:open_port({spawn, Cmd},[exit_status]),
    loop(Port,[], Timeout).
    
loop(Port, Data, Timeout) ->
    receive
        {Port, {data, NewData}} -> loop(Port, Data++NewData, Timeout);
        {Port, {exit_status, 0}} -> Data;
        {Port, {exit_status, S}} -> throw({commandfailed, S})
    after Timeout ->
        throw(timeout)
    end.