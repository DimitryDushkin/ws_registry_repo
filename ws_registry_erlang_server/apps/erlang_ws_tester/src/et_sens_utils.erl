%%% Author: ddushkin
%%% Created: 23.03.2012
%%% Description: Main module of testing site's sensivity
%%% edited with throughput test style
-module(et_sens_utils).

%%
%% Include files
%%

%%
%% Exported Functions
%%
-export([send_requests/4, send_requests/5]).

%%
%% API Functions
%%
-define(USER_AGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8; ru-RU)
					 AppleWebKit/535.19 (KHTML, like Gecko)
					 Chrome/18.0.1025.151 Safari/535.19").


%%--------------------------------------------------------------------
%% Start sending request using test plan set in arguments
%% Result is written to log file via descriptor
%% Arguments: (Incremential step, Max. request, URL, Log file descriptor)
%% Returns: none
%%--------------------------------------------------------------------
send_requests(TestIncrementStep, MaxRequests, URL, FileDescriptor) ->
	send_requests(TestIncrementStep, 0, MaxRequests, URL, FileDescriptor).


%% finish when LastRequestsToSend == MaxRequests
%% Loop over all iteration with increasing requests per second 
send_requests(_, LastRequestsToSend, MaxRequests, _, _) 
  when LastRequestsToSend == MaxRequests -> 
	error_logger:info_msg("Test is over!"),	
	os:cmd("afplay './notify.mp3'"),
	ok; 
	
send_requests(TestIncrementStep, LastRequestsToSend, MaxRequests, URL, FileDescriptor) ->
	% 1. Count how much to send next requests
	% 	 X(t) = a + X(t-1)
	%	 where X(t) – number of requests to send at time 't'
	RequestsToSend = TestIncrementStep + LastRequestsToSend,
	
	% 2. Parallel send request via pmap	
	IterationNumber = round(RequestsToSend/TestIncrementStep),
	
	% 2.1 Make scheldue of request within one iteration


	% 2.1. Prepare data for parallel execution
	% DataToSend <- [RequestsToSend | URL | Delay to send | Iteration number]
	DataToSend = lists:map(fun(RequestNumber) ->
								% Flow of requests has uniform distribution within a second
								% Задержка равномерно распределена в рамках секунды
								Delay = round(RequestNumber * (1000 / RequestsToSend)), 
								{RequestsToSend, URL, Delay, IterationNumber}	   		   
						   end, lists:seq(0, RequestsToSend)),
	% 2.2. Parallel send
	Log = utils_common:pmap(fun send_request/1, DataToSend),
	
	% 3. Write test's result to log
	disk_log:balog(FileDescriptor, Log),
	
	% 4. Wait for random amount of time.
	%	 Mostly depends on amount of sent requests.
	%	 It's done for protection of DDOS detection.
	%    Example: if ReqToSend == 100, sleep <- {0, 10000} ms
	TimeToSleep = 5 * random:uniform(100) * RequestsToSend,
	
	error_logger:info_msg(
	 "~p requests sent and received.
	 Iteration ~p/~p.
	 WS: ~p.
	 Wait for ~p ms to next iteration.",
	 [
	  RequestsToSend,
	  round(RequestsToSend/TestIncrementStep),
	  round(MaxRequests/TestIncrementStep),
	  URL, TimeToSleep
	 ]
	),
	timer:sleep(TimeToSleep),
	
	% 5. Send next banch on requests
	?MODULE:send_requests(TestIncrementStep, RequestsToSend, MaxRequests, URL, FileDescriptor).


%%======================================================================
%% Local functions
%%======================================================================
send_request({RequestsToSend, URL, Delay, IterationNumber}) ->
	% 0-1000 ms delay before sending request
	% Flow of requests has uniform distribution within a second
	timer:sleep(Delay),
	
	ReqSendTime = utils_common:get_timestamp(),
    
    try 
      et_http_utils:get_waiting_time(URL, 30000) 
    of
      {WaitingTime, _TimeToSend} ->
        lists:concat([ReqSendTime, ",", WaitingTime, ",", IterationNumber, ",", RequestsToSend, "\n"])
    catch % any errors
      _:_ -> 
        lists:concat([ReqSendTime, ",0,", IterationNumber, ",", RequestsToSend, "\n"])
    end.