%% @doc Web front-end of OverLoader.
-module(et_web_face).
-export([start_link/0, stop/0, handle_http/1]).



start_link() ->
	error_logger:info_msg("Web face has started \n"), 
	misultin:start_link([{port, 11101},
						 {loop, fun(Request) -> handle_http(Request) end}]).


stop() ->
	misultin:stop().

handle_http(Req) ->
	handle(Req:get(method), Req:resource([lowercase, urldecode]), Req).


% ---------------------------- \/ handle REST --------------------------------------------------------------

% handle a GET on /
handle('GET', [], Req) ->
	Req:ok([{"Content-Type", "text/plain"}], "Erlang web-sevices' tester is here!");

% handle a GET on /sync-services
handle('GET', ["sync-services"], Req) ->
	Reply = gen_server:call(et_sync_server, sync_services), 
	Req:ok([{"Content-Type", "text/plain"}], "Ok: ~p", [Reply]);

% handle a GET on /service
handle('GET', ["service"], Req) ->
	Req:ok([{"Content-Type", "text/plain"}], "Services in queue: ....");

% handle a GET on /service/{ServiceForeingId}
% 1. Add service to queue. It can present there already.
handle('GET', ["service", ServiceForeingId], Req) ->
	Req:ok([{"Content-Type", "text/plain"}], "This is ~s service page.", [ServiceForeingId]);

% handle the 404 page not found
handle(_, _, Req) ->
	Req:respond(404, "Page not found.").

% ---------------------------- /\ handle REST --------------------------------------------------------------