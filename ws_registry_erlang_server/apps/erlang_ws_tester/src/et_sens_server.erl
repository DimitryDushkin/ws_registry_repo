%%% -------------------------------------------------------------------
%%% Author  : ddushkin
%%% Description :
%%%
%%% Created : 18.08.2012
%%% -------------------------------------------------------------------
-module(et_sens_server).

-behaviour(gen_server).
%% --------------------------------------------------------------------
%% Include files
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% External exports
-export([start_link/1, stop_server/0, process_ws/1, start_test_once/1, standalone_start/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(state, {test_queue :: list(),
				finished_tests :: list(),
				server_status :: string(),
				current_ws = "" :: string(),
				current_test_iteration_number = 1 :: integer()}).

-define(SERVER, ?MODULE).
-define(TEST_PLAN, [%{max_req_per_second, 2},
					%{test_step, 1},
					{max_req_per_second, 250},				 % Maximum requests per second
					{test_step, 10},						 % Value to increase req.per.second
															 % for next iteration
					{test_iterations_interval, 9*60*60*1000},% In milliseconds. Next test every 9 hours
					{total_tests, 3},					% 3 tests total => 9*3=27 hours
					{avg_test_duration, 30*60*1000}]). 	% In ms. Average test longs 30 mins.

%% ====================================================================
%% External functions
%% ====================================================================

%% @doc Starts the server.
-spec start_link([list()]) -> {ok, pid()}.
start_link(WSs) ->
    gen_server:start_link({local, ?SERVER}, ?SERVER, [WSs], []).

%% @doc Stops the server.
stop_server() ->
	gen_server:call(?SERVER, stop).

standalone_start() ->
	Filename = filename:join([filename:dirname(code:which(?MODULE)), "..", "test", "sites_for_test.txt"]),
	{ok, WSs} = file:consult(Filename),
	et_sens_server:start_link(WSs).

%% ====================================================================
%% Server functions
%% ====================================================================

%% --------------------------------------------------------------------
%% Function: init/1
%% Description: Initiates the server
%% Returns: {ok, State}          |
%%          {ok, State, Timeout} |
%%          ignore               |
%%          {stop, Reason}
%% --------------------------------------------------------------------
init([WSs]) ->
	
	% 2. @see handle_info/3 timeout
    {ok, #state{test_queue = WSs, server_status = "init"}, 0}.	

%% --------------------------------------------------------------------
%% Function: handle_call/3
%% Description: Handling call messages
%% Returns: {reply, Reply, State}          |
%%          {reply, Reply, State, Timeout} |
%%          {noreply, State}               |
%%          {noreply, State, Timeout}      |
%%          {stop, Reason, Reply, State}   | (terminate/2 is called)
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------
handle_call(get_state, _From, State) -> 
	{reply, State, State};

handle_call(stop, _From, State) ->
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% --------------------------------------------------------------------
%% Function: handle_cast/2
%% Description: Handling cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------
handle_cast(_, State) ->
    {noreply, State}.

%% --------------------------------------------------------------------
%% Function: handle_info/2
%% Description: Handling all non call/cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------

%% @doc Continue startup routine.
%% @see init/0 for beginning of startup process.
handle_info(timeout, State) ->
	% 3. Get server operational mode
	% Filename = filename:join([
	% 				filename:dirname(code:which(?MODULE)), "..", "server_mode.txt"]),
	% {ok, [{Server_mode}]} = file:consult(Filename),

	% 4. Switch to proper operational mode
	Server_mode = "test_day",
	case Server_mode of
		"test_day" ->
			?SERVER ! init_test_day;
		"test_once" ->
			?SERVER ! init_test_once;
		_ ->
			ok
	end,
	

    {noreply, State};


%% ===========================================================================
%% @doc Make tests on all WSs
handle_info(init_test_day, #state{test_queue = Web_services4test} = State) -> 
	% 2. Check list is empty (all tests are done)
	case Web_services4test of
		[] -> 
			% 3*. Stop server
			{stop, normal, State};
		_ ->
			% 3*. Count number of WSs for next global interval
			% One global iteration = 27 hours by default (9*3)
			N = round(
					proplists:get_value(test_iterations_interval, ?TEST_PLAN) /
					proplists:get_value(avg_test_duration, ?TEST_PLAN)),
			
			Number_of_WSs_in_process = if
				% Осталось веб-сервисов меньше, чем надо для одной глоб. итерации
				N > length(Web_services4test)  ->
					length(Web_services4test);
				N =< length(Web_services4test) ->
					N
			end,

			
			% 4. Get first N web-services from Web_services4test
			% and put rest WS in Web_services4test_rest
			{WSs_in_process, Web_services4test_rest} = 
				lists:split(Number_of_WSs_in_process, Web_services4test), 


			% 5. Сделаем расписание тестов для всех WSs_in_process.
			% *foldl позволяет между "перечислением элементов в массиве" хранить и
			% изменять одну перменную. В моем случае – Time.
			Total_tests = proplists:get_value(total_tests, ?TEST_PLAN), 	% Just shorthand
			Test_interval = proplists:get_value(test_iterations_interval, ?TEST_PLAN),
			lists:foldl(
				fun({WS}, Time) -> 
					% Каждый WS должен будет пройти 1 тест каждые 9 часов,
					% всего 3 теста, т.е. первый сервис пройдет 1 тест в 0-00
					% 2 тест в 9-00 и 3 – в 18-00;
					% 2-ой веб-сервис пройдет 1 тест в 0-30, 2 – 9-30, 3 – 18-30
					% и т.д.
					lists:map(
						fun(I) ->
							Next_test_start_time = I * Test_interval + Time,
							% Set beginning of test
							timer:send_after(Next_test_start_time,
								?SERVER, {start_test_day, WS}) 
						end, lists:seq(0, Total_tests-1)),

					% Returned variable become Time on next iteration
					Time + proplists:get_value(avg_test_duration, ?TEST_PLAN)
		   		end, 1000, WSs_in_process),

			% Start next global test on next bunch in 27 hours
			timer:send_interval(Total_tests*Test_interval, ?SERVER, init_test_day),

			% 6. Set status
			Server_status = lists:concat(["it's a day test! WSs in process:",
							io_lib:format("~p", [WSs_in_process])]), 
			{noreply, State#state{server_status = Server_status,
								  test_queue = Web_services4test_rest}}
	end;


%% @doc Start new test on WS
handle_info({start_test_day, WS}, State) ->
	spawn(?SERVER, process_ws, [WS]),

	{noreply, State#state{current_ws = WS}};
%% ===========================================================================

%% ===========================================================================
%% @doc Make test once for all web-services in list
handle_info(init_test_once, #state{test_queue = Web_services4test} = State) ->
	
	% 2. Check list is empty
	case Web_services4test of
		[] -> 
			% 3*. Stop server
			{stop, normal, State};
		_ ->
			% 3*. Get web-service from list
			[WS_data | Web_services4test_rest] = Web_services4test,

			Ws = proplists:get_value(test_uri, WS_data), 
			% 4. Start the test
			spawn(?SERVER, start_test_once, [Ws]),

			% 5. Set status
			Server_status = "working on single test per web-service",
			{noreply, State#state{server_status = Server_status,
								  current_ws = Ws,
								  test_queue = Web_services4test_rest}}
	end.

start_test_once(Ws) ->
	% Позволим функции ловить сигналы о завершении процесса
	process_flag(trap_exit, true),
	% Создадим link (связь) с процессом, исполняющим функцию process_ws
	Pid = spawn_link(?SERVER, process_ws, [Ws]),
	% Подождем пока связанный процесс завершится
	receive
		% При завершении процесса отправим на вычисление
		% чувствительности и начнем новый тест
		{'EXIT', Pid, _Reason} ->
			compute_sensitivity(Ws),
			?SERVER ! init_test_once
	end.
%% ===========================================================================




%% --------------------------------------------------------------------
%% Function: terminate/2
%% Description: Shutdown the server
%% Returns: any (ignored by gen_server)
%% --------------------------------------------------------------------
terminate(_Reason,  _State) ->
	application:stop(ol), 
	ok.

%% --------------------------------------------------------------------
%% Func: code_change/3
%% Purpose: Convert process state when code is changed
%% Returns: {ok, NewState}
%% --------------------------------------------------------------------
code_change("0", State, _Extra) ->
    {ok, State}.

%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------
process_ws(URL) ->
	% 1. Make common file's name end part
	{{Year,Month,Day}, {Hour,Minutes,Seconds}} = erlang:localtime(),
	{ok, {_, _, Domain, _, _, _}} = utils_http_uri:parse(URL),
	Time = lists:flatten(
			io_lib:format("~4.10.0B~2.10.0B~2.10.0B-~2.10.0B~2.10.0B~2.10.0B-",
		   		[Year, Month, Day, Hour, Minutes, Seconds])),
	CommonFileNamePart = lists:concat([Time, Domain]),
	process_ws(URL, CommonFileNamePart).

process_ws(URL, CommonFileNamePart) ->
	% 2. Set test plan
	Max_rps = proplists:get_value(max_req_per_second, ?TEST_PLAN),
	Test_step = proplists:get_value(test_step, ?TEST_PLAN),
	error_logger:info_msg("Current web-service: ~p. Max. requests: ~p.~n",
						   [URL, Max_rps]),
	
	% 3. Init disk logger				
	LogFileName = lists:concat([CommonFileNamePart, ".csv"]),
	LogFileDestination = filename:join(
							[filename:dirname(code:which(?MODULE)),	"..", "logs", LogFileName]
						 ),
	{ok, FileDescriptor} = disk_log:open([{name, LogFileName},
										  {file, LogFileDestination},
										  {format, external}]),
	% 4. Start sending requests
	et_sens_utils:send_requests(Test_step, Max_rps, URL, FileDescriptor),

	% 5. End of test
	% Close log file's descriptor
	disk_log:close(FileDescriptor).

compute_sensitivity(_Ws) ->
	ok.	
