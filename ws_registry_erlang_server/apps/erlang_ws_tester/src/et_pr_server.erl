%%%-------------------------------------------------------------------
%%% @author Dmitry Dushkin
%%% @doc
%%%   Performance and realibility properties tester.
%%%   Perform tests to get values of
%%%   - latenct (mean and std.)
%%%   - mtbf
%%%   - mttr
%%%   - availability
%%%   - reliability
%%%   - uptime
%%%   - downtime
%%% @end
%%% @TODO: store uptime, downtime in HOURS
%%%-------------------------------------------------------------------
-module(et_pr_server).

-behaviour(gen_server).

%% API
-export([ start_link/0, 
          stop_server/0, 
          make_test/1,
          compute_features/3
        ]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3,
         mean_std/1]).


-define(TEST_INTERVAL, 1000*60*30). % in ms
-define(TEST_INTERVAL_HOURS, ?TEST_INTERVAL / 3600000).


-define(REQUESTS_NUMBER, 5).

-define(USER_AGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8; ru-RU)
   AppleWebKit/535.19 (KHTML, like Gecko)
   Chrome/18.0.1025.151 Safari/535.19").

-record(state, {test_queue :: list(),
                current_ws = "" :: string()}).

-include("et_record_service.hrl").

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
        gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% @doc Stops the server.
stop_server() ->
  gen_server:call(?MODULE, stop).
%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    inets:start(),
    
    % 1. @see handle_cast/2 startup
    gen_server:cast(self(), startup), 
    {ok, #state{}}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------

%% @doc Continue startup routine.
%% @see init/0 for beginning of startup process.
handle_cast(startup, State) ->
    % every 30 mins gen_server:cast(self(), start_test) 
    gen_server:cast(self(), start_test),
    timer:apply_interval(?TEST_INTERVAL, gen_server, cast, [self(), start_test]),

    {noreply, State};


handle_cast(start_test, State) ->
  % 1. Get services
  {ok, Services} = et_db:get_services_local_list(),
  
  % 2. Start new test
  make_test(Services),

  {noreply, State#state{test_queue = Services}}.
  

%% @doc Основная функция вычисления значения критериев.
%% Перебирает все сервисы во входящем списке.
-spec make_test(list(#service{})) -> ok.
make_test([]) ->
  error_logger:info_msg("End of test iteration. Next iteration in ~p mins", [?TEST_INTERVAL/(1000*60)]), 
  ok;

make_test([CurrentService | RestServices]) ->
  % 1. Send requests
  {Latencies, ErrorsNumber} 
    = lists:mapfoldl(
      fun(_, Acc) ->
        try 
          et_http_utils:get_waiting_time(CurrentService#service.rest_test_uri) 
        of
          {WaitingTime, _TimeToSend} ->
            {WaitingTime, Acc}
        catch % any errors
          _:_ -> 
            {0, Acc + 1}
        end
      end, 0, lists:seq(1, ?REQUESTS_NUMBER)),
   
  % Все пять запросов = одна проверка.
  % Если все запросы ошибочные, значит сервис лежит
  ServiceToSave = compute_features(CurrentService, Latencies, ErrorsNumber),

  error_logger:info_msg("Service to save: ~p~n", ServiceToSave), 
  et_db:save_service(ServiceToSave),
  
  make_test(RestServices).



%% @doc Universal command-patter-like function for computing features
%%      using test results.
%% @TODO: можно вынести в отдельные функции повторяющиеся куски 
%%        с вычислением задержки и доступности
-spec compute_features(Service :: #service{},
                       Latencies :: list(), 
                       ErorrsNumber :: integer()) -> #service{}.
% 1.   Не работает, полностью 
% 1.1. До этого работал, но теперь --- нет,
%      либо первое измерение.
% tested
compute_features(Service, _Latencies, ErrorsNumber) when
  (ErrorsNumber == ?REQUESTS_NUMBER) and (Service#service.state /= off) ->
  
  % Compute MTBF 
  N = Service#service.mtbf_quantity + 1,
  Uptime = Service#service.uptime,
  Prev_MTBF = Service#service.mtbf,

  MTBF = ((N - 1) * Prev_MTBF + Uptime) / N,
  MTBF_quantity = N,

  % Compute availability
  case Service#service.mttr of
    0.0 -> Availability = 0.0;
    _ -> Availability = MTBF / (MTBF + Service#service.mttr)
  end,

  Service#service{
    state = off,
    uptime = 0.0,
    downtime = ?TEST_INTERVAL_HOURS,
    mtbf = MTBF,
    mtbf_quantity = MTBF_quantity,
    availability = Availability
  };


% 1.2  До этого тоже не работал
compute_features(Service, _Latencies, ErrorsNumber) when
  (ErrorsNumber == ?REQUESTS_NUMBER) and (Service#service.state == off) ->
  
  % Compute downtime
  Service#service{
    downtime = Service#service.downtime + ?TEST_INTERVAL_HOURS
  };


% 2.   Работает, могут быть ошибки
% 2.1. До этого не работал, а теперь работает,
%      либо первое измерение.
compute_features(Service, Latencies, ErrorsNumber) when
  (ErrorsNumber /= ?REQUESTS_NUMBER) and (Service#service.state /= on) ->
  
  % Compute MTTR  
  N = Service#service.mttr_quantity + 1,
  Downtime = Service#service.downtime,
  Prev_MTTR = Service#service.mttr,
  MTTR = ((N - 1) * Prev_MTTR + Downtime) / N,
  MTTR_quantity = N,
  
  % Compute latency
  NonZeroLatencies = [ Lat || Lat <- Latencies, Lat > 0 ],
  {Mean_latency, Std_latency} = mean_std(NonZeroLatencies),

  % Compute availability
  case Service#service.mtbf of
    0.0 -> Availability = 1.0;
    _ -> Availability = Service#service.mtbf / (Service#service.mtbf + MTTR)
  end,


  % Compute reliability
  PrevRel = Service#service.reliability,
  CurrentRel = (?REQUESTS_NUMBER - ErrorsNumber) / ?REQUESTS_NUMBER,
  M = Service#service.reliability_quantity + 1,
  Reliability = ((M - 1) * PrevRel + CurrentRel) / M,
  Reliability_quantity = M,

  Service#service{
    state = on,
    uptime = ?TEST_INTERVAL_HOURS,
    downtime = 0.0,
    mttr = MTTR,
    mttr_quantity = MTTR_quantity,
    mean_latency = Mean_latency,
    std_latency = Std_latency,
    availability = Availability,
    reliability = Reliability,
    reliability_quantity = Reliability_quantity
  };


% 2.2. До этого тоже работал
compute_features(Service, Latencies, ErrorsNumber) when
  (ErrorsNumber /= ?REQUESTS_NUMBER) and (Service#service.state == on) ->
  
  % Compute latency
  NonZeroLatencies = [ Lat || Lat <- Latencies, Lat > 0 ],
  {Mean_latency, Std_latency} = mean_std(NonZeroLatencies),

  % Compute reliability
  PrevRel = Service#service.reliability,
  CurrentRel = (?REQUESTS_NUMBER - ErrorsNumber) / ?REQUESTS_NUMBER,
  M = Service#service.reliability_quantity + 1,
  Reliability = ((M - 1) * PrevRel + CurrentRel) / M,
  Reliability_quantity = M,

  % Compute uptime
  Service#service{
    uptime = Service#service.uptime + ?TEST_INTERVAL_HOURS,
    mean_latency = Mean_latency,
    std_latency = Std_latency,
    reliability = Reliability,
    reliability_quantity = Reliability_quantity
  }.




%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(_Request, _From, State) ->
        Reply = ok,
        {reply, Reply, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------



handle_info(_Info, State) ->
     {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
        ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
        {ok, State}.

%%%===================================================================
%%% Export functions
%%%===================================================================




%%%===================================================================
%%% Internal functions
%%%===================================================================
%% @doc more efficient implimentation from udacity statistics
mean_std([]) ->
    {0,0};

mean_std(List) ->
    N = length(List),
    Sum = lists:sum(List),
    SumOfSqrs = lists:sum([X*X || X <- List]),
    Mean = Sum / N,
    Variance = SumOfSqrs / N - (Sum * Sum) / (N * N),
    Std = math:pow(Variance, 0.5),
    {Mean, Std}.