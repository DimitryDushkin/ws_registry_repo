%% @doc Database abstraction layer

% TODO: now only for REST services

-module(et_db).


-export([
		 init/0,
		 install_mnesia/0,

		 get_services_remote_list/0,
		 get_services_local_list/0,
		 sync_services_list/0,
		 
		 save_service/1,

		 put_latency/2,
		 put_availability/2,
		 put_sensitivity/2,
		 store_availability_data/4,
		 get_availability_data/1
		 
		]).


-define(APP_URL, "http://localhost:8080/api/").

-record(ws_availability_data,
		    {service_id :: pos_integer(),
		     total_reqs :: pos_integer(),
		     success_reqs :: pos_integer(),
		     availability :: number()}).

-include("et_record_service.hrl").


%% ====================================================================================
%% ============================ Initialization ==================================================
init() ->
	error_logger:info_msg("Starting mnesia"),
	inets:start(),
	App_root = filename:dirname(filename:dirname(code:which(?MODULE))),
	application:set_env(mnesia, dir, filename:join(App_root, "mnesia_db")),
	mnesia:start(),
	case mnesia:wait_for_tables([service], 2000) of
		ok -> ok;
		{_, _Error} -> 
			% @INFO: Schema can be created if only mnesia itself is stopped
			mnesia:stop(),
			install_mnesia(),
			mnesia:start()
	end.

	

install_mnesia() ->
	error_logger:info_msg("Installing mnesia tables' schemas"), 
	App_root = filename:dirname(filename:dirname(code:which(?MODULE))),
	application:set_env(mnesia, dir, filename:join(App_root, "mnesia_db")),
	% Setup dir with mnesia files
    Nodes = [node()],
    mnesia:create_schema(Nodes),
    application:start(mnesia),
    mnesia:create_table(service,
					    [{attributes, record_info(fields, service)},
					     {disc_copies, Nodes}]),
    application:stop(mnesia).





%% ============================= Retriving from remote service ===================

%%--------------------------------------------------------------------
%% @doc
%% Get remote services' list
%%--------------------------------------------------------------------
-spec get_services_remote_list() -> {ok, list()} | {error, string()}.
get_services_remote_list() ->
	case httpc:request(?APP_URL ++ "function/") of 
		{ok, {_, _, Body}} ->  
			{ok, Services} = json:decode(Body),
			RemoteServices = 
		        [ 
		         [
		          {service_key, binary_to_list(proplists:get_value(<<"id">>, Service))},
		          {rest_test_uri, binary_to_list(proplists:get_value(<<"rest_test_uri">>, Service))},
		          {rest_method, binary_to_atom(proplists:get_value(<<"rest_method">>, Service), utf8)}
		         ]
		          || {Service}  <- lists:flatten(Services)
		        ],
			{ok, RemoteServices};
		_ -> {error, "Cannot connect to server"}
	end.


%%--------------------------------------------------------------------
%% @doc
%% Get services' list from local DB
%%--------------------------------------------------------------------
-spec get_services_local_list() -> {ok, list(#service{})} | {error, string()}.
get_services_local_list() ->
	Services = 
		mnesia:dirty_match_object(mnesia:table_info(service, wild_pattern)),
	{ok, Services}. 


%%--------------------------------------------------------------------
%% @doc
%% Sync local *list* of functions with remote list in Google App Engine
%%--------------------------------------------------------------------
-spec sync_services_list() -> ok | error.
sync_services_list() ->
	error_logger:info_msg("Starting syncing services"),

	% 1. Get remove services' list
	{ok, RemoteServices} = get_services_remote_list(),
	
    % 2. Add new services
    KeysOfServicesInDb = mnesia:dirty_all_keys(service),
  
	lists:foreach(fun(S) ->
		Key = proplists:get_value(service_key, S), 
		
		% @TODO: can be faster if *dict* will be used
		% returns true if there is such key
		case utils_common:list_find(Key, KeysOfServicesInDb) of
			true -> ok;
			false ->
				error_logger:info_msg("Service Added: ~p", [S]),
				mnesia:dirty_write(#service{
	    			service_key = proplists:get_value(service_key, S),
	    			rest_test_uri = proplists:get_value(rest_test_uri, S),
	    			rest_method = proplists:get_value(rest_method, S)
	    		}) 
	    end
	end, RemoteServices), 


    % 3. Remove local services are not in remote list
	lists:foreach(fun(ServiceKey) ->
		case utils_common:proplist_find(service_key, ServiceKey, RemoteServices) of
			true -> ok;

			% if not found, then delete it from DB
			false ->
				% @INFO: argument is {<table_name>, <PrimaryKeyValue>}
				mnesia:dirty_delete({service, ServiceKey}),
				error_logger:info_msg("Service Deleted: ~p", [ServiceKey])
		end   		
	end, KeysOfServicesInDb),
    

	ok.


%%--------------------------------------------------------------------
%% @doc
%% Save service to local DB and remote one
%%--------------------------------------------------------------------
-spec save_service(list(#service{})) -> boolean().
save_service(Service) ->
	% 1. Save to local DB
	mnesia:dirty_write(Service#service{}),

	% 2. Save to remote DB
	{ok, Request_body} = 
		json:encode({[
					  {mtbf, round_or_null(Service#service.mtbf)},
					  {mttr, round_or_null(Service#service.mttr)},
					  {availability, float_to_binary(Service#service.availability)},
					  {reliability, float_to_binary(Service#service.reliability)},
					  {uptime, float_to_binary(Service#service.uptime)},
					  {downtime, float_to_binary(Service#service.downtime)},
					  {mean_latency, round_or_null(Service#service.mean_latency)},
					  {std_latency, round_or_null(Service#service.std_latency)}
					]}), 
	ok = send_put_request(Service#service.service_key, Request_body),
	ok.



round_or_null(Value) when is_float(Value) or is_integer(Value) ->
	round(Value);

round_or_null(_) ->
	0.



%% ============================ Iternal functions =====================
send_put_request(ServiceId, Request_body) ->
	% Sync store value
	Response = httpc:request(put,
		 			  {lists:concat([?APP_URL, "function/", ServiceId]), % url
		 			   [],	% headers
		 			   "application/json",	% content type
		 			   	Request_body % body
		 			  },
		 			  [],	% HTTP options
		 			  []	% Options
		 			 ),
	case Response of
		{ok, _} -> ok;
		{error, Reason} -> {error, Reason}
	end.










%% ====================================================================================
%% ============================= OLD CODE WILL BE REMOVED =============================
%% ====================================================================================


%% ====================================================================================
%% ============================= Saving to remove service =============================
%% ====================================================================================
put_latency(ServiceId, {MeanLatency, StdLatency}) ->
	{ok, Request_body} = 
		json:encode({[
					  {mean_latency, round(MeanLatency)},
					  {std_latency, round(StdLatency)}
					]}),
	send_put_request(ServiceId, Request_body).
	

put_availability(ServiceId, Value) ->	
	{ok, Request_body} = 
		json:encode({[{availability, list_to_binary(Value)}]}),
	send_put_request(ServiceId, Request_body).


put_sensitivity(ServiceId, Value) ->
	{ok, Request_body} = 
		json:encode({[{sensitivity, list_to_binary(Value)}]}),
	send_put_request(ServiceId, Request_body).



%% ====================================================================================
%% ============================= Local DB operations ==================================
%% ====================================================================================
store_availability_data(ServiceId, TotalReqs, SuccReqs, Availability) ->
	T = fun() ->
		mnesia:write(#ws_availability_data{
				service_id = ServiceId,
			    total_reqs = TotalReqs,
			    success_reqs = SuccReqs,
			    availability = Availability	
			})
	end,
	case mnesia:transaction(T) of
		{atomic, ok} -> ok;
		{aborted, Reason} -> {error, Reason}
	end.


get_availability_data(ServiceId) ->
	T = fun() ->
		mnesia:read({ws_availability_data, ServiceId})
	end,
	case mnesia:transaction(T) of
		{atomic, [Row]} -> {ok, Row};
		{aborted, Reason} -> {error, Reason}
	end.
