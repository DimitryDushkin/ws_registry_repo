-module(et_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    error_logger:info_msg("Erlang ws tester booting..."),
    et_db:init(),
    et_sup:start_link().

stop(_State) ->
    ok.

% File map:
% et_pr_server (performance & reliability) --- gen_server for computing values of other properties (latency)
%				availability, reliability, etc.).
%				Booted on app start.			
% et_db --- DB adapter for Mnesia DB
% et_http_utils --- HTTP utils for getting waiting time of request processing
% et_sens --- gen_server for computing a value of sensitivity
% et_sens_utils --- utils for sending batch of requests and saving to log
% et_web_face --- RESTful API to all functions of this web-service
% et_sync_server --- gen_server for syncing web-services list with remote server