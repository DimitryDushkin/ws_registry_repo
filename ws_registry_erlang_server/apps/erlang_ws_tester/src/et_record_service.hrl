-record(service,
        {
            service_key :: string(),    % primary key
            rest_test_uri :: string(),
            rest_method :: 'get' | 'post' | 'put' | 'delete',
            state :: 'undefined' | 'on' | 'off',
            mtbf = 0.0 :: number(),
            mtbf_quantity = 0 :: number(),
            mttr = 0.0 :: number(),
            mttr_quantity = 0 :: number(),
            availability = 1.0 :: number(),
            reliability = 1.0 :: number(),
            reliability_quantity = 0 :: number(),
            uptime = 0.0 :: number(),
            downtime = 0.0 :: number(),
            mean_latency :: number(),
            std_latency :: number()
        }).