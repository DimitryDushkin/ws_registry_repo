-module(et_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	Et_sync_server = ?CHILD(et_sync_server, worker),
	Et_pr_server = ?CHILD(et_pr_server, worker),
	Wb_face = ?CHILD(et_web_face, worker),
    {ok, { {one_for_one, 5, 10}, [Et_sync_server, Et_pr_server, Wb_face]} }.