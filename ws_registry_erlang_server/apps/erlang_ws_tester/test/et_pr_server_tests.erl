-module(et_pr_server_tests).
-include_lib("eunit/include/eunit.hrl").

-include("../src/et_record_service.hrl").




% smoke_test() ->
% 	et_db:init(),
% 	%et_sync_server:start_link(),
% 	?assertMatch({ok, _}, et_pr_server:start_link()),
% 	?assertMatch(ok, et_pr_server:stop_server()).

compute_features_1_1_test() ->
	et_db:init(),

	% 1. All reqs. fail
	CurrentService = #service{},
	Latencies = [0, 0, 0, 0, 0],
	ErrorsNumber = 5,
	ComputedService = et_pr_server:compute_features(CurrentService, Latencies, ErrorsNumber),
	%error_logger:info_msg("~p", [ComputedService]),
	
	ServiceToGet = #service{
		state = off,
		mtbf_quantity = 1,
		availability = 0.0,
		downtime = 0.5
	},
	?assertEqual(ServiceToGet, ComputedService). 



compute_features_1_2_test() ->
	et_db:init(),

	% 1. All reqs. fail
	CurrentService = #service{
		state = off
	},
	Latencies = [0, 0, 0, 0, 0],
	ErrorsNumber = 5,
	ComputedService = et_pr_server:compute_features(CurrentService, Latencies, ErrorsNumber),

	ServiceToGet = #service{
		state = off,
	    downtime = 0.5
	},
	?assertEqual(ServiceToGet, ComputedService).


% 2.   Работает, могут быть ошибки
% 2.1. До этого не работал, а теперь работает
compute_features_1_2_2_test() ->
	et_db:init(),

	% 1. All reqs. fail
	CurrentService = #service{
		state = off
	},
	Latencies = [80, 90, 100, 110, 120],
	ErrorsNumber = 0,
	ComputedService = et_pr_server:compute_features(CurrentService, Latencies, ErrorsNumber),

	{Mean_latency, Std_latency} = et_pr_server:mean_std(Latencies),
	ServiceToGet = #service{
		state = on,
		uptime = 0.5,
	    mttr = 0.0,
	    mttr_quantity = 1,
	    reliability_quantity = 1,
	    mean_latency = Mean_latency,
	    std_latency = Std_latency
	},
	?assertEqual(ServiceToGet, ComputedService).

% testing_test() ->
% 	et_db:init(),
% 	{ok, Services} = et_db:get_services_local_list(),
% 	?assertEqual(ok, et_pr_server:make_test(Services)). 