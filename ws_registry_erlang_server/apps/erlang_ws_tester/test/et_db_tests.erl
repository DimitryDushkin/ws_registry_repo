%% Author: ddushkin
%% Created: 27.06.2012
-module(et_db_tests).
-include_lib("eunit/include/eunit.hrl").


% @INFO: all tests end with "_test"
% In "<...>_test()" function ?assert<...> must be used, instead of ?_assert<...>

sync_services_list_test() ->
	et_db:init(),
	et_db:sync_services_list().

get_local_services_test() ->
	et_db:init(),
	?assertMatch({ok, _}, et_db:get_services_local_list()).

save_service_test() ->
	et_db:init(),
	{ok, [Service | _]} = et_db:get_services_local_list(),
	?assertMatch(ok, et_db:save_service(Service)).