-module(et_http_utils_tests).

-include_lib("eunit/include/eunit.hrl").

new_test() ->
	?assertMatch({_, _}, et_http_utils:get_waiting_time("http://46.137.94.37:11111/2s_script.php")),
	?assertMatch({_, _}, et_http_utils:get_waiting_time("http://forums.ag.ru/?board=discuss&action=post_comment&source=news&id=21913")).