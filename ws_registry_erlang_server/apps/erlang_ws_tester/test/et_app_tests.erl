%% Author: ddushkin
%% Created: 27.06.2012
-module(et_app_tests).
-include_lib("eunit/include/eunit.hrl").

%%
%% API Functions
%%
app_smoke_test() ->
	?assertEqual(ok, application:start(erlang_ws_tester)),
	?assertEqual(ok, application:stop(erlang_ws_tester)). 