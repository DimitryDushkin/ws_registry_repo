%% Author: ddushkin
%% Created: 27.06.2012
-module(et_sens_server_tests).
-include_lib("eunit/include/eunit.hrl").


startup_test() ->
	% 1. Get web-services list in '/../test/sites.txt'
	Filename = filename:join([filename:dirname(code:which(?MODULE)), "..", "test", "sites_for_test.txt"]),
	{ok, WSs} = file:consult(Filename),
	et_sens_server:start_link(WSs). 



