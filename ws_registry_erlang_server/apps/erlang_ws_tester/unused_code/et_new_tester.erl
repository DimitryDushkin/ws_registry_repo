-module(et_new_tester).

% et_new_tester:show_tcp_timing().

-compile(export_all).

% -define(USER_AGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8; ru-RU)
%    AppleWebKit/535.19 (KHTML, like Gecko)
%    Chrome/18.0.1025.151 Safari/535.19").


collect_statistics() ->
    Data = lists:map(fun(_) ->
                        {TimeToConnect, WaitingTime} = 
                           % show_tcp_timing("blog-sky2high.rhcloud.com", "/etc/2s_script.php"),
                           %show_tcp_timing("surdoserver.ru", "/"),
                           %show_tcp_timing("www.ag.ru", "/"),
                           show_tcp_timing("46.137.94.37", "/2s_script.php", 11111),
                        lists:concat([TimeToConnect, ",", WaitingTime, "\n"]) 
                     end, lists:seq(0, 1000)),
    LogFileName = "connecting_waiting_latencies.csv",
    LogFileDestination = filename:join(
                            [filename:dirname(code:which(?MODULE)), "..", "logs", LogFileName]
                         ),
    file:write_file(LogFileDestination, Data),
    error_logger:info_msg("End of collecting of statistics."), 
    ok.


show_tcp_timing(Host, Query) ->
    show_tcp_timing(Host, Query, 80).

show_tcp_timing(Host, Query, HTTPPort) ->
	error_logger:info_msg("URL: ~p~p", [Host, Query]),
	TimeOut = 10000,

	StartTime = utils_common:get_timestamp(),

    % 1. Get IP by hostname
    {ok, IpAdr} = inet:getaddr(Host, inet),
    TimeToDNSLookUp = utils_common:get_timestamp() - StartTime ,
    error_logger:info_msg("1. Time to DNS look up: ~w ms", [TimeToDNSLookUp]),

    % 2. Make TCP connection
    BeforeConnectTime = utils_common:get_timestamp(),
    {ok, Socket} = gen_tcp:connect(IpAdr, HTTPPort, 
           	                      [binary,
           	                       {packet, http},
           	                       {active, false},
                  				   {reuseaddr, true}]),
    TimeToConnect = utils_common:get_timestamp() - BeforeConnectTime,
    error_logger:info_msg("2. Time to connect: ~w ms", [TimeToConnect]),
    
    % 3. Send HTTP request
    Request = io_lib:format("GET ~s HTTP/1.1\r\nHost: ~s\r\n\r\n", [Query, Host]),
    gen_tcp:send(Socket, list_to_binary(Request)),
    
    TimeToSend = utils_common:get_timestamp() - StartTime
    			 - TimeToDNSLookUp - TimeToConnect,
    error_logger:info_msg("3. Time to send: ~w ms", [TimeToSend]),
    

    % ============= THIS IS SERVICE TIME FOR ONLY FIRST PACKET  =============
    %4. Receive HTTP responce
    BeforeReceiveTime = utils_common:get_timestamp(),
    case gen_tcp:recv(Socket, 0, TimeOut) of 
    	{ok, Resp} ->
    		%WaitingTime = utils_common:get_timestamp() - StartTime
    		%	 		  - TimeToDNSLookUp - TimeToConnect - TimeToSend,
            WaitingTime = utils_common:get_timestamp() - BeforeReceiveTime,
   			error_logger:info_msg("4. Waiting Time: ~w ms", [WaitingTime]),
    		
    		error_logger:info_msg("Response: ~p", [Resp]), 
    		% 4.1. Get pure request processing time
            error_logger:info_msg("4. Pure request processing time: ~w ms", [WaitingTime - TimeToConnect]),

            % 5. Close TCP connection
    		gen_tcp:close(Socket),
    		
    		TimeToClose = utils_common:get_timestamp() - StartTime
    			 		  - TimeToDNSLookUp - TimeToConnect - TimeToSend
    			 		  - WaitingTime,
   			error_logger:info_msg("5. Time to close: ~w ms", [TimeToClose]),
            
            % return
            {TimeToConnect, WaitingTime};
    	{error, closed} ->
    		gen_tcp:shutdown(Socket, write), 
    		error_logger:error_msg("gen_tcp receive error"),
            
            % return
            http_error
    end.









    % ==================================================================


    % ========== THIS IS SERVICE TIME FOR ALL PACKETS ==============

    % do_recv(Socket),
    % WaitingTime = utils_common:get_timestamp() - StartTime
    %                      - TimeToDNSLookUp - TimeToConnect - TimeToSend,
    % error_logger:info_msg("4. Waiting Time: ~w ms", [WaitingTime]),

    % ==================================================================

    % === Compare to inets ====
    % inets:start(),
    % StartTime2 = utils_common:get_timestamp(),
    % Response = httpc:request(get,
    %                           {"http://" ++ Host ++ "/", []},
    %                           [{timeout, 30000}], []),
    % InetsWaitTime = utils_common:get_timestamp() - StartTime2,
    % error_logger:info_msg("Inets req-resp time: ~p ms ~n Response: ~n",
    %  						[InetsWaitTime]).
    


% do_recv(Socket) ->
%     case gen_tcp:recv(Socket, 0, 30000) of
%         {ok, _Response} ->
%             %error_logger:info_msg("Response: ~p", [Response]),
%             gen_tcp:shutdown(Socket, write),
%             do_recv(Socket); 
%         {error, closed} ->
%             gen_tcp:close(Socket),
%             error_logger:info_msg("Close socket"), 
%             closed;
%         {error, Reason} ->
%             error_logger:info_msg("gen_tcp error: ~p", [Reason])
%     end.
    