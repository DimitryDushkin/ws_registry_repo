-module(et_new_tester_tests).

-include_lib("eunit/include/eunit.hrl").

new_test() ->
	%?assertMatch({_, _}, et_new_tester:show_tcp_timing("surdoserver.ru", "/")).
	%?assertEqual({_, _}, et_new_tester:show_tcp_timing("sky2high.net", "/")).
	%?assertEqual({_, _}, et_new_tester:show_tcp_timing("sky2high.net", "/etc/1s_script.php")).
	%?assertEqual({_, _}, et_new_tester:show_tcp_timing("blog-sky2high.rhcloud.com", "/etc/2s_script.php")).
	%?assertEqual({_, _}, et_new_tester:show_tcp_timing("46.137.94.37", "/2s_script.php", 11111)).
	?assertEqual(ok, et_new_tester:collect_statistics()).