## Система мониторинга и выбора композиции веб-сервисов с учётом предпочтений 

На основе описываемых в [диссертационной работе](http://goo.gl/ch14d4)  моделей, методов и алгоритмов разработан
программный комплекс на базе технологий Python, Erlang и JavaScript. В качестве технологической
площадки размещения системы (хостинга) используется облачное решение вида "платформа как услуга"
(Platform as a Service, PaaS) Google App Engine, что позволяет без существенных финансовых затрат
предоставлять публичный доступ к системе при условии невысокого числа активных пользователей.

Разработанный программный комплекс позволяет:

*   вести реестр веб-сервисов и их функций,
*   проводить постоянный мониторинг значений характеристик веб-сервисов,
*   предоставляет удобный графический интерфейс для моделирования задач и выбора оптимальных по предпочтениям композиций веб-сервисов, реализующих соответствующие задачи. 


Данная система распространяется на условиях свободного исходного кода (open-source).

Демонстрация системы – [http://ws-decisioner.appspot.com/#/decisioner](http://ws-decisioner.appspot.com/#/decisioner).

Обзор основных компонент:

*   ws_registry_gae – Google App Engine base web-applicaiton for storing information of web-services. Also via we_registry_erlang_server possible to collect some non-functional properties.
*   ws_registry_erlang_server – Erlang based server for collecting non-function properties of web-services.
*   ws_registry_sensitivity - Python based module for computing sensetivity

Currently non-functional properties:

*   latency (mean and std.)
*   mtbf
*   mttr
*   availability
*   reliability
*   uptime
*   downtime
