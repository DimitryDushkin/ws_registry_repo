""" 
	Get sensitivity class of web-service using 
	SVM model
"""
import sys
import argparse

# 0. Get ws_name or show documentation of this cmd programm
parser = argparse.ArgumentParser(
			description='Get sensitivity class of web-service using SVM model.')
parser.add_argument('ws_name', metavar='ws_name', help='Web-service name')
args = parser.parse_args()

# ==============================================================================
MODEL_PATH = "./svm_model.pkl"
TESTS_RESULTS_FOLDER = "../ws_registry_erlang_server/apps/erlang_ws_tester/"

# 1. Get ws_name
ws_name = args.ws_name


