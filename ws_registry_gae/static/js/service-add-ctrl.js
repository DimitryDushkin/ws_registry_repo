function ServiceAddCtrl($scope, $http, $element, $rootScope, $location, Service) {
	$scope.submit = function () {
		var form = $element;

		Service.save(form.serializeObject(), function(data) {
			// Hide modal
			$('#addServiceModal').modal('hide');
			// Clear form
			form[0].reset();
			$scope.services.push(data);
			$scope.currentService = data;
		});
	}
}