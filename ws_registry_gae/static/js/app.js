//#TODO: 1. merge services list with service details to reduce quiries number
// 2. Write lot of tests!
// 3. ui.bootstrap has only tooltip

angular.module('registry',
		 [//'ngAnimate',
		 	'registryRESTService',
		  'taskRESTService',
		  'subtaskRESTService',
		  'functionRESTService',
		  'ui.bootstrap'])
	.config(['$routeProvider', function($routeProvider) {
		// TODO: rewrite this to use ng-view pattern
		// on view for one module
		$routeProvider.
			when('/registry',
			 {
			  templateUrl: '/static/partials/registry.html',
			  controller: RegistryCtrl
			 });
		$routeProvider
			.when('/decisioner', {
				templateUrl: '/static/partials/tasks.html',
				controller: TasksCtrl
			})
			.otherwise({redirectTo: '/decisioner'});
    }])
    .run(function($rootScope, $location){
		// Function for setting active navbar menu item
		$rootScope.getClass = function(path) {
		    var current_path = $location.path().substr(0, path.length);
		    if (current_path == path) {
		        if($location.path().substr(0).length > 1 && path.length == 1 )
		            return "";
		        else
		            return "active";
		    } else {
		        return "";
		    }
		}
    });



// Dependecies
//ServicesCtrl.$inject = ['$scope', '$http'];
//ServiceDetailCtrl.$inject = ['$scope', '$routeParams', '$http'];