function manageFunctionCtrl($scope, $rootScope, Service, Function) {

	/**
	 * Just show and hide add function form
	 * @todo Can be included in template and removed from here
	 * 
	 * @return {undefined}
	 */
	$scope.showHideEditFunctionForm = function() {$scope.edit_function_mode = !$scope.edit_function_mode}
	
	/**
	 * Update web-service's function data via JSON server API
	 * @param  {Object} fun
	 * @return {undefined}
	 */
	$scope.updateRESTFunction = function(fun) {
		Function.update({functionId: fun.key}, fun, function(){
			$scope.showHideEditFunctionForm();
		});
	}

	/**
	 * Remove web-service's function via API call.
	 * @param  {Object} fun
	 * @return {undefined}
	 */
	$scope.removeFunction = function(fun) {
		var idx = $scope.currentService.functions.indexOf(fun);
		Function.remove({functionId: fun.key}, function(){
			$scope.currentService.functions.splice(idx, 1);
		});
	}
}