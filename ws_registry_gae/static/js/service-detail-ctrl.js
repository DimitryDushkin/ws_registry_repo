function ServiceDetailCtrl($scope, $rootScope, $element, Service, Function) {
	var $container = $('.service-details');
	$scope.addedFunction = {};
	$scope.service_edit_mode = false;
	$scope.add_function_mode = false;

	$scope.showHideEditServiceForm = function() {$scope.service_edit_mode = $scope.service_edit_mode ? false : true}

	$scope.updateService = function() {
		// currentService -- is instance of ngResource
		// so it has all methods defined in services.js
		$scope.currentService.$update(function() {
			$scope.edit_mode = false;
			$scope.showHideEditServiceForm();		
		});
	}

	$scope.remove = function() {
		$scope.currentService.$remove(function(){
			// remove from array
			var idx = $scope.services.indexOf($scope.currentService);
			$scope.services.splice(idx, 1);
			$scope.unsetCurrentService();
		});
	}

	$scope.loadDetails = function() {
		if (!$scope.currentService)
			return ''
		else
			switch ($scope.currentService.ws_type) {
				case 'wsdl':
					return '/static/partials/service-details-wsdl.html';
				case 'rest':
					return '/static/partials/service-details-rest.html';
			}
	}



	$scope.refreshData = function () {
		$scope.currentService['status'] = 'proc';
		var funs = $scope.currentService.functions;
		$scope.currentService.$refresh_data(function(data) {	// success
		 	// Because functions wasn't changed and data do not have
		 	// 'functions', so it should not be replaced
		 	//$scope.currentService.functions = funs;
		 }, function() {	// error
			$scope.currentService.status = 'down';
			$rootScope.alertMessage = 'WSDL URI неверна или сервис недоступен.';
		});
	}


	// ========================= REST function =============================


	$scope.rest_methods = ['get', 'post', 'put', 'delete'];
	$scope.addedFunction.rest_method = 'get';
	$scope.showHideAddFunctionForm = function() {$scope.add_function_mode = $scope.add_function_mode ? false : true}
	
	$scope.showHideEditFunctions = function() {$scope.edit_functions_list_mode = $scope.edit_functions_list_mode ? false : true}

	$scope.addRESTFunction = function() {
		if(!$scope.currentService.functions) 
			$scope.currentService.functions = [];
		
		var func = angular.copy($scope.addedFunction);
		$scope.currentService.functions.push(func);
		Service.update(
			{serviceId: $scope.currentService.id},
			$scope.currentService, function(data){
				$scope.showHideAddFunctionForm();
				$scope.addedFunction = {};
				func.key = data.key;
			}
		);
	}






	// ========================= WSDL =============================
	$scope.wsdlContentVisible = false;
	$scope.toggleWsdlContent = function () {
		$('.wsdl-content').slideToggle()
		$scope.wsdlContentVisible = !$scope.wsdlContentVisible ? true : false
	}




}