function RegistryCtrl($scope, Service) {
	$scope.currentService = null;
	$scope.services = Service.list();
	$scope.setCurrentService = function (idx) {
		$scope.currentService = $scope.services[idx];
	}

	$scope.unsetCurrentService = function() {$scope.currentService = null}
}