// REST services

angular.module('registryRESTService', ['ngResource'])
    .factory('Service', function($resource){
  		return $resource('/api/service/:serviceId/', {serviceId: '@id'}, 
      {
  			save: {url: '/api/service/?', method: 'POST'},
        remove: {method: 'DELETE'},
        update: {method: 'PUT'},
        list: {url: '/api/service/?', method:'GET', isArray: true},
        refresh_data: {method: 'PUT', params: {process_ws: true}},
  		  //refresh_properties: {method: 'PUT', params: {refresh_properties: true}},
        get_all_functions: {url: '/api/service/?', method: 'GET', isArray: true, params: {get_all_functions: true}}
  		})
});

angular.module('functionRESTService', ['ngResource'])
    .factory('Function', function($resource) {
      return $resource('/api/function/:functionId/', {functionId: '@functionId'},
        {
          list: {url: '/api/function/?', method:'GET', isArray: true},
          remove: {method: 'DELETE'},
          updateRealizations: {method: 'PUT'},
          update: {method: 'PUT'}
        })
});

angular.module('taskRESTService', ['ngResource'])
  	.factory('Task', function($resource) {
  		return $resource('/api/task/:taskId/', {taskId: '@taskId'}, 
      {
  			save: {url: '/api/task/?',	method: 'POST'},
        update: {method: 'PUT'},
  			list: {url: '/api/task/?', method: 'GET', isArray: true},
        remove: {method: 'DELETE'},
        choosePareto: {method: 'GET', isArray: true, params: {'choose_pareto': true}},
        choosePreferenceAware: {method: 'PUT', isArray: true, params: {'choose_preference_aware': true}},
  		})
});

angular.module('subtaskRESTService', ['ngResource'])
    .factory('SubTask', function($resource) {
      return $resource('/api/subtask/:subtaskId/', {subtaskId: '@subtaskId'},
        {
          remove: {method: 'DELETE'},
          updateRealizations: {method: 'PUT'},
          update: {method: 'PUT'}
        })
});