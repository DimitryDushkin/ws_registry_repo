"use strict";
function taskDecisionerCtrl($scope, Task) {
	$scope.paretoVariants = null;
	$scope.preferenceAwareVariants = null;
	$scope.togglePrefAwareDetails = function() {$('.prefAwareDetails').slideToggle();};

	$scope.compensations = [
		{r_name: 'Чувствительность',																		name: 'sensitivity', 	scale: 'класс', value: 1, min: 1, max: 5,	 	step: 1,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Доступность',																					name: 'availability', 	scale: '',	value: 0, min: 0, max: 1,	 	step: 0.001, preferred: 0, nonpreferred: 0},
		{r_name: 'Среднее время до отказа (MTBF)',											name: 'mtbf', 			scale: 'ч', 	value: 0, min: 0, max: 10000,	step: 100,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Среднее время восстановления (MTTR)',									name: 'mttr', 			scale: 'ч', 	value: 0, min: 0, max: 10000,	step: 100,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Вероятность корректной работы',												name: 'reliability', 	scale: '',	value: 0, min: 0, max: 1,	 	step: 0.001, preferred: 0, nonpreferred: 0},
		{r_name: 'Время работы с последнего сбоя (начала наблюдения)',	name: 'uptime', 		scale: 'ч', 	value: 0, min: 0, max: 10000,	step: 100,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Среднее время обработки запроса',											name: 'mean_latency', 	scale: 'мс', 	value: 0, min: 0, max: 30000,	step: 100,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Стандартное отклонение времени обработки запроса',		name: 'std_latency', 	scale: 'мс', 	value: 0, min: 0, max: 10000,	step: 100,	 preferred: 0, nonpreferred: 0},
		{r_name: 'Стоимость одного запроса',														name: 'cost', 			scale: 'цент $',value: 0, min: 0, max: 0.01,	step: 0.0001,preferred: 0, nonpreferred: 0},
		{r_name: 'Число бесплатных запросов в день',										name: 'free_day_limit',	scale: 'шт', 	value: 0, min: 0, max: 100000,	step: 1000,	 preferred: 0, nonpreferred: 0}
	];


	$scope.choosePareto = function() {
		Task.choosePareto(
			{taskId: $scope.currentTask.id}, function(data){
				// Replace pareto_set functions with normal one
				
				angular.forEach(data, function(st){					
					for (var st_f_key in st.pareto_set) {
						var searchResult = $.grep($scope.functions,
							 	 function(f) {
							 	 	return f.id == st.pareto_set[st_f_key].key 
							 	 });
						st.pareto_set[st_f_key] = searchResult[0];
					}
				});

				$scope.paretoVariants = data;
			});
	}


	$scope.choosePreferenceAware = function() {
		var requestData = $scope.compensations;
		
		Task.choosePreferenceAware(
			{taskId: $scope.currentTask.id}, requestData, function(data){
			angular.forEach(data, function(st){					
					for (var st_f_key in st.pareto_set) {
						var searchResult = $.grep($scope.functions,
							 	 function(f) {
							 	 	return f.id == st.pareto_set[st_f_key].key 
							 	 });
						st.pareto_set[st_f_key] = searchResult[0];
					}
				});

				$scope.preferenceAwareVariants = data;
		});
	}

	$scope.setPreferred = function(compensation) {
		angular.forEach($scope.compensations, function(c) {
			c.preferred = 0;
		});

		compensation.preferred = 1;
		compensation.nonpreferred = 0;
	};

	$scope.setNonPreferred = function(compensation) {
		compensation.preferred = 0;
		compensation.nonpreferred = 1;
	};

	$scope.resetPreferred = function(c) {
		c.preferred = 0;
		c.nonpreferred = 0;
	};

	
}