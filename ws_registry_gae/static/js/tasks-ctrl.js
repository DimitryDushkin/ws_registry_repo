// Task -- REST resource
// @see app.js

function TasksCtrl($scope, $http, $routeParams, Task, Function) {
	$scope.functions = [];
	$scope.tasks = Task.list(function() {
		//get all functions
		$scope.functions = Function.list(function(){
			// Fill tasks with functions-realizations
			angular.forEach($scope.tasks, function(t){
				angular.forEach(t.subtasks, function(st) {
					angular.forEach(st.functions, function(st_f_id) {
						var searchResult = $.grep($scope.functions,
							 	 function(f) {
							 	 	return f.id == st_f_id 
							 	 });
						st_f_id = searchResult[0];
					});
				});
			});
		});
	});
	
	$scope.setCurrentTask = function(task) {
		delete $scope.currentTask; //for animation purpose
		$scope.currentTask = task
	};
	$scope.unsetCurrentTask = function() {$scope.currentTask = ""};
	$scope.showAddTaskForm = function() {$('#addTaskForm').slideToggle()};
	$scope.toggleDecisioner = function() {$('.decisioner-container').slideToggle()}
}


/**
  * Update, remove, add subtask
  */
function manageTaskCtrl($scope, $http, Task) {
	$scope.saveTask = function() {
		$scope.editMode = false;
		var data = {"taskId": $scope.currentTask.id,
					"name": $scope.currentTask.name};
		Task.update(data);
	}

	/**
	  * idx -- is array index of object
	  */
	$scope.remove = function() {
		Task.remove({"taskId": $scope.currentTask.id}, function() {
			// remove from array
			idx = $scope.tasks.indexOf($scope.currentTask);
			$scope.tasks.splice(idx, 1);
			$scope.unsetCurrentTask();
		});
	}

	$scope.addSubTask = function() {
		if(!$scope.currentTask.subtasks) 
			$scope.currentTask.subtasks = [];
		var totalItems = $scope.currentTask.subtasks.length
		var subtask = {"name": "Подзадача " + totalItems++};
		$scope.currentTask.subtasks.push(subtask);

		// Save to db
		var data = {"taskId": $scope.currentTask.id,
					"subtask": subtask};
		Task.update(data, function(data) {
			// Save subtask id from DB
			subtask['id'] = data.id;
		});
	}
}


/**
 * Add task
 */
function addTaskCtrl($scope, $http, $element, Task) {
	$scope.submit = function() {
		var form = $element;

		Task.save(form.serializeObject(), function(data) {
			// Hide modal
			$('#addTaskForm').slideToggle();
			// Clear form
			form[0].reset();
			// add to tasks list
			var task = {name: $scope.task_name,
						id: data.id};
			$scope.tasks.push(task);
			$scope.currentTask = task;
		});
	}
}


/**
  * Remove subtask
  * При открытии интерфейса управления реализациями надо вычитать
  * из общего списка (слева unselectedFunctions = копия functions) те функции,
  * которые есть в списке справа (chosenFunctions)
  */
function manageSubTaskCtrl($scope, $http, SubTask) {
	// Left list
	$scope.unselectedFunctions = $scope.functions.slice(0);
	// Selected in left list
	$scope.selectedFunctions = [];
	// Right list
	$scope.chosenFunctions = [];

	function init() {
		var functions_keys = $scope.subtask.functions;
		var transferred_function = {};
		for (var function_key in functions_keys) {
			for (var array_key in $scope.unselectedFunctions) {
				if ($scope.unselectedFunctions[array_key].id == functions_keys[function_key]) {
					transferred_function = $scope.unselectedFunctions.splice(array_key, 1);
					$scope.chosenFunctions = $scope.chosenFunctions.concat(transferred_function);
					break;
				}
			}
		}
	}
	init();

	$scope.remove = function(subtask) {
		// remove from DB
		SubTask.remove({'subtaskId': subtask.id});		
		// remove from array
		var idx = $scope.currentTask.subtasks.indexOf(subtask);
		$scope.currentTask.subtasks.splice(idx, 1);
	}

	$scope.showHideEditSubtaskForm = function(subtask) {subtask.editSubtaskMode = subtask.editSubtaskMode ? false : true}
	$scope.saveSubTask = function(subtask) {
		SubTask.update({'subtaskId': subtask.id, 'name': subtask.name});
		$scope.showHideEditSubtaskForm(subtask);
	}

	$scope.moveRealizationsToList = function(idx) {
		// add to chosen functions selected ones
		$scope.chosenFunctions = $scope.chosenFunctions.concat(
									$scope.selectedFunctions);
		// remove chosen functions from list of all functions
		for (var key in $scope.selectedFunctions) {
			$scope.unselectedFunctions.splice(
				$scope.unselectedFunctions.indexOf($scope.selectedFunctions[key]), 1)
		}

		$scope.selectedFunctions = [];

	}

	$scope.removeRealizationfromList = function(idx) {
		$scope.unselectedFunctions = $scope.unselectedFunctions.concat(
										$scope.chosenFunctions.splice(idx, 1));
	}

	$scope.saveChosenRealizations = function(subtask) {
		subtask['realizations'] = $scope.chosenFunctions;
		var dataToSend = {
						  'subtaskId': subtask.id,
						  'realizations': subtask['realizations']
					     };
		SubTask.updateRealizations(dataToSend);

		$scope.editRealizationsMode = false;
	}


}