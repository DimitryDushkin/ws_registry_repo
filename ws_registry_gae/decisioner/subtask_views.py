# coding:utf-8

import webapp2
import json

from google.appengine.ext import db

from decisioner.models import SubTask
from utils.utils import return_json


class ApiHandler(webapp2.RequestHandler):


    @return_json
    def put(self, subtask_key):
        st = SubTask.get(subtask_key)
        dict_request = json.loads(self.request.body)
        if 'realizations' in dict_request:
            r_keys = [db.Key(encoded=r['id']) for r in dict_request['realizations']]
            st.functions = r_keys
            st.put()
            self.response.write('ok')

        if 'name' in dict_request:
            st.name = dict_request['name']
            st.put()
            self.response.write('ok')


    @return_json
    def delete(self, subtask_key):
        try:
            st = SubTask.get(subtask_key)
            st.delete()
            self.response.write('ok')
        except Exception, e:
            self.response.set_status(500)
            self.response.write(e)
            self.response.write(subtask_key)
