# coding:utf-8

import json

from decisioner.models import Task
from decisioner.models import SubTask
from registry.models import Function

import numpy as np

# 1  – bigger – better
# -1 – lower  – better
PROPERTIES_TRENDS = {
    'sensitivity': -1,
    'availability': 1,
    'mtbf': 1,
    'mttr': -1,
    'reliability': 1,
    'uptime': 1,
    'mean_latency': -1,
    'std_latency': -1,
    'cost': -1,
    'free_day_limit': 1}


def choose_pareto(task_id):
    task = Task.get_by_id(long(task_id))
    subtasks = SubTask.all().ancestor(task)

    # 0. get pure dict without GAE classes
    subtasks_dict = convert_subtasks_to_dict(subtasks)

    # Now we have all objects
    # 1. Standartize all data – for all properties bigger value is better
    for st in subtasks_dict:
        for f in st['functions']:
            for property_name, property_value in f.iteritems():
                if property_name not in PROPERTIES_TRENDS:
                    continue

                f[property_name] = property_value * PROPERTIES_TRENDS[property_name]
            # /end for each property in function
        # /end for each function
    # /end for each subtask

    # 2. Get pareto set for each subtask
    for st in subtasks_dict:
        st['pareto_set'] = returnPareto2(st['functions'])
        st.pop('functions')

    # 3. Prepare data to show
    return json.dumps(subtasks_dict)


def convert_subtasks_to_dict(subtasks):
    subtasks_dict = []
    for st in subtasks:
        tmp_functions = []
        for f_key in st.functions:
            func = Function.get(f_key)
            tmp_functions.append(func.get_properties_and_key())

        st = st.to_dict()
        st['functions'] = tmp_functions
        subtasks_dict.append(st)

    return subtasks_dict


def returnPareto2(subtaskFunctions):
    pareto_set = []
    # 1. Create matrix with values of properties
    arr = []
    for f in subtaskFunctions:
        vals = []
        for property_name, property_value in f.iteritems():
            if property_name not in PROPERTIES_TRENDS:
                continue
            vals.append(float(property_value))
        arr.append([vals])

    arr = np.array(arr)
    # 2. Now compare each function with others
    for i, a in enumerate(arr):
        others = range(0, len(arr))
        others.remove(i)
        tmp = arr[others, :]
        max_values = np.max(tmp, axis=0)
        max_compare_result = a > max_values  # ">" or ">=" ?
        #print max_compare_result
        # if .any() == true, mean that function is not worse then others functions
        if (max_compare_result).any():
            pareto_set.append(subtaskFunctions[i])

    return pareto_set


def choose_preference_aware(task_id, compensations):
    """
        compensations -- multidict (webob)
    """
    compensations = json.loads(compensations)
    task = Task.get_by_id(long(task_id))
    subtasks = SubTask.all().ancestor(task)

    # -1. Get dict with nonpref properties and their compensation values
    nonpreferred_properties_compensations = {}
    for c in compensations:
        if c['nonpreferred'] == 1:
            # Размер убыли
            nonpreferred_properties_compensations[c['name']] = float(c['value'])

    # 0. get pure dict without GAE classes
    subtasks_dict = convert_subtasks_to_dict(subtasks)

    # 1. Apply compensations and standartize functions' prop values

    for st in subtasks_dict:
        for f in st['functions']:

            # 1.1. Get V_1 -- compensation for preferred propery
            # and K_1(V) --- value of property for preferred property
            preferred_property_name, preferred_property_compensation, preferred_property_value = \
                                        get_preferred_compensation_and_value(f, compensations)

            #print '---'
            for property_name, property_value in f.iteritems():
                if property_name not in PROPERTIES_TRENDS:
                    continue

                # standartize
                f[property_name] = f[property_name] * PROPERTIES_TRENDS[property_name]

                if property_name in nonpreferred_properties_compensations:
                    # apply compensations
                    # K_2(V) = V_2 * K_1(V) + V_1 * K_2(V)
                    #print "{} * {} + {} * {} = {}".format(V_0, d[1], V_1, d[0], V_0 * d[1] + V_1 * d[0])
                    f[property_name] = nonpreferred_properties_compensations[property_name] * preferred_property_value \
                                        + preferred_property_compensation * f[property_name]

            # /end for each property in function
        # /end for each function
    # /end for each subtask

    # 2. Get pareto set for each subtask
    for st in subtasks_dict:
        #print "start st {}".format(st['id'])
        st['pareto_set'] = returnPareto2(st['functions'])
        st.pop('functions')

    # 3. Prepare data to show
    return json.dumps(subtasks_dict)


def get_preferred_compensation_and_value(func, compensations):
    preferred_property_name = ''
    preferred_property_compensation = 0
    preferred_property_value = 0
    for c in compensations:
        if c['preferred'] == 1:
            preferred_property_name = c['name']
            preferred_property_compensation = float(c['value'])
            break

    for f_property_name, f_property_value in func.iteritems():
        if f_property_name == preferred_property_name:
            preferred_property_value = float(f_property_value) * PROPERTIES_TRENDS[f_property_name]
            break

    # return  K, V_1, K_1(V)
    return preferred_property_name, preferred_property_compensation, preferred_property_value
