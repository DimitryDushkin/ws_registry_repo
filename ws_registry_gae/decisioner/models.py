# coding:utf-8
from google.appengine.ext import db


class Task(db.Model):
    name = db.StringProperty(required=True, verbose_name=u'Название задачи')


class SubTask(db.Model):
    name = db.StringProperty(required=True, verbose_name=u'Название подзадачи')
    functions = db.ListProperty(db.Key, verbose_name=u'Возможные реализации функциями ВСов')

    def to_dict(self):
        """ Include numeric ID and correct parse functions' list"""
        #functions = db.get(self.functions)
        tmp = {	'id': str(self.key()),
                'name': self.name,
                'functions': [str(f) for f in self.functions]}
        return tmp
