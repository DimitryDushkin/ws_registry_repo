# coding:utf-8

import webapp2
import json

from google.appengine.ext import db

from decisioner.models import Task
from decisioner.models import SubTask
from decisioner.optimal_finder import choose_pareto
from decisioner.optimal_finder import choose_preference_aware
#from registry.models import Function

from utils.utils import return_json


class ApiHandler(webapp2.RequestHandler):

    @return_json
    def get(self, *args):
        """ Get list of task and subtasks """

        if self.request.get('choose_pareto'):
            response = choose_pareto(args[0])
            self.response.write(response)
            return

        items = Task.all().run()
        response = []
        for item in items:
            data = db.to_dict(item, {"id": str(item.key().id())})
            subtasks = SubTask.all().ancestor(item)
            data['subtasks'] = [s.to_dict() for s in subtasks]
            response.append(data)

        self.response.write(json.dumps(response))


    @return_json
    def post(self, *args):
        """ Save new task. Return new object """
        task_json = json.loads(self.request.body)
        task = Task(name=task_json['name'])  # Dict to attrs
        task.put()
        self.response.write(json.dumps(db.to_dict(task, {"id": str(task.key().id())})))



    @return_json
    def put(self, id):
        if self.request.get('choose_preference_aware'):
            response = choose_preference_aware(id, self.request.body)
            self.response.write(response)
            return

        task = Task.get_by_id(int(id))
        dict_request = json.loads(self.request.body)
        if 'name' in dict_request:
            task.name = dict_request['name']
            try:
                task.put()
                self.response.write('ok')
            except Exception, e:
                self.response.set_status(500)
                self.response.write(e)

        # if there is subtask
        if 'subtask' in dict_request:
            subtask = SubTask(name=dict_request['subtask']['name'], parent=task)
            subtask.put()
            self.response.write(json.dumps({'id': str(subtask.key())}))


    @return_json
    def delete(self, id):
        try:
            Task.get_by_id(long(id)).delete()
            self.response.write('ok')
        except Exception, e:
            self.response.set_status(500)
            self.response.write(e)
