#just snippets

# create bulkloader.yaml
appcfg.py create_bulkloader_config --filename=bulkloader.yaml --url=http://ws-decisioner.appspot.com/_ah/remote_api -e legato.di@gmail.com

# upload
appcfg.py upload_data --url=http://ws-decisioner.appspot.com/_ah/remote_api --config_file=bulkloader.yaml --filename=services_and_params.csv --kind=Function -e legato.di@gmail.com

#upload web-services
load_upload_data ddushkin$ appcfg.py upload_data --url=http://ws-decisioner.appspot.com/_ah/remote_api --config_file=bulkloader.yaml --filename=web_services.csv --kind=WebService -e legato.di@gmail.com

# download from app server in csv format
appcfg.py download_data --config_file=bulkloader.yaml --filename=tasks.csv --kind=Task --url=http://ws-decisioner.appspot.com/_ah/remote_api -e legato.di@gmail.com
