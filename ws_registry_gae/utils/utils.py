# ============== Utils ==============================
def return_json(handler_method):
    """ Decorator to return JSON Content-Type """
    def add_json_header(self, *args, **kwargs):
        handler_method(self, *args, **kwargs)
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
    return add_json_header
# ============== END Utils ===========================