# coding:utf-8

import webapp2
import json
import logging

from google.appengine.ext import db

from decisioner.models import Task
from decisioner.models import SubTask
from registry.models import WebService, WSDLWebService, RESTWebService, Function
from utils.utils import return_json



class ImportDataHandler(webapp2.RequestHandler):
    def post(self):
        all_data = json.loads(self.request.get('uploaded_file'))
        tasks = all_data['tasks']
        webservices = all_data['webservices']
        for t in tasks:
            task_instance = Task(name=t['name'])
            task_instance.put()
            for st in t['subtasks']:
                subtask_instance = SubTask(parent=task_instance, name=st['name'])
                subtask_instance.put()

        for ws in webservices:
            if ws['ws_type'] == 'rest':
                ws_instance = RESTWebService(name=ws['name'])
            else:
                ws_instance = WSDLWebService(name=ws['name'])
            # interate ws keys
            for k, v in ws.items():
                if (k == 'functions') or (k == 'ws_type'):
                    continue
                if hasattr(ws_instance, k):
                    setattr(ws_instance, k, v)
            # /interate ws keys
            ws_instance.put()

            for f in ws['functions']:
                f_instance = Function(parent=ws_instance, name=f['name'])
                for k, v in f.items():
                    model_properties = f_instance.properties()
                    if hasattr(f_instance, k):
                        model_property = model_properties.get(k)
                        if isinstance(model_property, db.FloatProperty):
                            v = float(v)
                        elif isinstance(model_property, db.IntegerProperty):
                            v = int(v)
                        setattr(f_instance, k, v)
                f_instance.put()

        self.response.write('Data imported!')




class ExportDataHandler(webapp2.RequestHandler):
    @return_json
    def get(self):
        items = Task.all()
        tasks = []
        for item in items:
            data = db.to_dict(item, {"key": str(item.key())})
            subtasks = SubTask.all().ancestor(item)
            data['subtasks'] = [s.to_dict() for s in subtasks]
            tasks.append(data)

        items = WebService.all()
        webservices = []
        for item in items:
            data = item.to_dict()
            funs = Function.all().ancestor(item)
            data['functions'] = [db.to_dict(f, {"key": str(f.key())}) for f in funs]
            webservices.append(data)

        response = {
            'tasks': tasks,
            'webservices': webservices
        }

        self.response.write(json.dumps(response, indent=2))


    def export_function(self, *args):
        """ Get list of all functions in CSV format """
        funs = Function.all()
        response = 'id, ws_name, name, rest_test_uri, rest_method, sensitivity, availability, mtbf, mttr, reliability, uptime, downtime, mean_latency, std_latency, cost, free_day_limit\n'
        for f in funs:
            response += '{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}\n'.format(str(f.key()), f.ws_name.encode('utf-8'), f.name.encode('utf-8'), f.rest_test_uri, f.rest_method, f.sensitivity, f.availability, f.mtbf, f.mttr, f.reliability, f.uptime, f.downtime, f.mean_latency, f.std_latency, f.cost, f.free_day_limit)
        self.response.write(response)
        self.response.headers['Content-Type'] = 'text/csv'