#!/usr/bin/env python

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), 'lib/'))

import webapp2


app = webapp2.WSGIApplication([
    ('/api/service/(.*)', 'registry.views.ApiHandler'),
    ('/api/task/(.*)', 'decisioner.views.ApiHandler'),
    ('/api/subtask/(.*)', 'decisioner.subtask_views.ApiHandler'),
    ('/api/function/(.*)', 'registry.function_views.ApiHandler'),
    ('/api/admin/import_data', 'admin.views.ImportDataHandler'),
    ('/api/admin/export_data', 'admin.views.ExportDataHandler'),

], debug=True)
