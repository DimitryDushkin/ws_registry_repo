# coding:utf-8

from google.appengine.ext import db
from google.appengine.ext.db import polymodel


class WebService(polymodel.PolyModel):
    WS_STATUS = ('up', 'down', 'proc')

    name = db.StringProperty(required=True, verbose_name=u'Название веб-сервиса')
    status = db.StringProperty(choices=WS_STATUS, verbose_name=u'Статус веб-сервиса')
    docs_url = db.LinkProperty(verbose_name='Документация к веб-сервису')
    api_key = db.StringProperty(verbose_name=u'API ключ')

    def update_with_dict(self):
        pass

    def to_dict(self):
        """ Include numeric ID """
        tmp = dict(self.__dict__['_entity'])
        del tmp['class']
        tmp['ws_type'] = self.ws_type
        tmp['id'] = str(self.key())
        return tmp

    @property
    def ws_type(self):
        return 'none'


class WSDLWebService(WebService):
    wsdl_url = db.LinkProperty(verbose_name=u'Ссылка на WSDL-документ')
    wsdl_content = db.TextProperty(verbose_name=u'Содержимое файла WSDL')
    wsdl_docs = db.TextProperty(verbose_name=u'Документация к WSDL')

    @property
    def ws_type(self):
        return 'wsdl'


class RESTWebService(WebService):
    rest_schema_url = db.LinkProperty(verbose_name=u'URL c JSON схемой веб-сервиса')

    @property
    def ws_type(self):
        return 'rest'


class Function(db.Model):
    F_TYPE = ('wsdl', 'rest')
    REST_METHOD = ('get', 'post', 'put', 'delete')

    name = db.StringProperty(required=True, verbose_name=u'Название функции')
    ws_name = db.StringProperty(verbose_name=u'Название веб-сервиса')
    documentation = db.StringProperty(multiline=True, verbose_name='Описание функции')
    f_type = db.StringProperty(choices=F_TYPE, verbose_name=u'Тип функции')

    # Type-specific data
    rest_method = db.StringProperty(choices=REST_METHOD, verbose_name=u'Тип метода')
    rest_test_uri = db.StringProperty(required=False, verbose_name=u'Ссылка для теста REST функции')
    wsdl_test_post_data = db.TextProperty(verbose_name=u'Набор данных для теста WSDL функции')

    # Properties
    sensitivity = db.IntegerProperty(verbose_name=u'Класс чувствительности веб-сервиса')
    availability = db.FloatProperty(verbose_name=u'Доступность веб-сервиса в %')
    mtbf = db.FloatProperty(verbose_name=u'Среднее время до отказа, ч')
    mttr = db.FloatProperty(verbose_name=u'Среднее время восстановления, ч')
    reliability = db.FloatProperty(verbose_name=u'Вероятность корректной работы функции')
    uptime = db.FloatProperty(verbose_name=u'Uptime, ч')
    downtime = db.FloatProperty(verbose_name=u'Downtime, ч')
    mean_latency = db.IntegerProperty(verbose_name=u'Cреднее время отклика')
    std_latency = db.IntegerProperty(verbose_name=u'Стандартное отклонение ср. времени отклика')
    cost = db.FloatProperty(verbose_name=u'Стоимость 1-го запроса в центах')
    free_day_limit = db.IntegerProperty(verbose_name=u'Число бесплатных запросов в день')

    def get_properties_and_key(self):
        """ Include key """
        tmp = {
        'key': str(self.key()),
        'sensitivity': self.sensitivity,
        'availability': self.availability,
        'mtbf': self.mtbf,
        'mttr': self.mttr,
        'reliability': self.reliability,
        'uptime': self.uptime,
        'mean_latency': self.mean_latency,
        'std_latency': self.std_latency,
        'cost': self.cost,
        'free_day_limit': self.free_day_limit}

        return tmp
