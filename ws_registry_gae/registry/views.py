# coding:utf-8

import webapp2
#import cgi
import json
import logging
#import urllib2

from google.appengine.ext import db

from registry.models import WebService
from registry.models import WSDLWebService
from registry.models import RESTWebService
from registry.models import Function
from registry import ws_processing

from utils.utils import return_json

ERLANG_SERVER_URL = "http://localhost:11101/"


class ApiHandler(webapp2.RequestHandler):

    @return_json
    def get_ws_list(self, *args):
        """ Get list of web-services """
        items = WebService.all().run()
        response = []
        for item in items:
            data = item.to_dict()
            funs = Function.all().ancestor(item)
            data['functions'] = [db.to_dict(f, {"key": str(f.key())}) for f in funs]
            response.append(data)

        self.response.write(json.dumps(response))


    @return_json
    def get(self, id):
        if id:
            # Get by id = key
            ws_entity = WebService.get(id)
            functions = Function.all().ancestor(ws_entity)
            dict_response = db.to_dict(ws_entity)
            dict_response['functions'] = [db.to_dict(f) for f in functions]
            self.response.write(json.dumps(dict_response))
        else:
            self.get_ws_list(self)


    @return_json
    def post(self, *args):
        """ Save new ws. Return new object """
        ws_json = json.loads(self.request.body)
        if ws_json['ws_type'] == 'wsdl':
            ws = WSDLWebService(**ws_json)  # Dict to attrs
        else:
            ws = RESTWebService(**ws_json)  # Dict to attrs
        ws.put()
        self.response.write(json.dumps(ws.to_dict()))


    @return_json
    def put(self, key):
        ws_instance = WebService.get(key)

        # =========  Get functions, WSDL content etc. for service ==========
        if self.request.get('process_ws'):
            try:
                json_response = ws_processing.process_ws(ws_instance)
            except Exception, e:
                logging.error(e)
                self.response.set_status(500)
                return self.response.write(e)

            # Return updated WS
            return self.response.write(json_response)

        # ========= Send request to erlang server ===========
        # elif self.request.get('refresh_properties'):

        #     import urllib2

        #     url = ERLANG_SERVER_URL + 'service/' + ws_instance.key().id()
        #     try:
        #         result = urllib2.urlopen(url)
        #         #doSomethingWithResult(result)
        #     except urllib2.URLError, e:
        #         self.response.set_status(400)
        #         self.response.write("Erorr: {}".format(e))

        # ============== Modify WS ========================
        else:
            dict_request = json.loads(self.request.body)
            # Update only with properties in input dict
            for k, v in dict_request.items():
                if k == 'functions':
                    # sync functions from front-end <-> to back-end
                    functions = v
                    for f in functions:
                        if 'key' not in f:
                            # if function has no key -> add to db
                            f['parent'] = ws_instance
                            f['ws_name'] = ws_instance.name
                            new_function_key = Function(**f).put()
                            self.response.write('{"key":"' +
                                                str(new_function_key) +
                                                '"}')
                    continue

                # if k == "availability":
                #     v = float(v)

                if (k == 'id') or (k == 'ws_type'):
                    continue

                if hasattr(ws_instance, k):
                    setattr(ws_instance, k, v)
            # /for
            ws_instance.put()

            if (self.request.get('include_functions')):
                functions = Function.all().ancestor(ws_instance)
                dict_ws_instance = ws_instance.to_dict()
                dict_ws_instance['functions'] = map(db.to_dict, functions)
                self.response.write(json.dumps(dict_ws_instance))


    @return_json
    def delete(self, key):
        try:
            ws = WebService.get(key)
            funs = Function.all().ancestor(ws)
            db.delete(funs)
            ws.delete()
            self.response.write('ok')
        except Exception, e:
            self.response.set_status(500)
            self.response.write(e)
