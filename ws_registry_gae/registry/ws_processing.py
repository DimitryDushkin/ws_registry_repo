# coding: utf-8
""" Methods, that get usefull info about REST and WSDL web-services """

import logging
import urllib2
import json

from registry.models import WebService
from registry.models import Function

from google.appengine.ext import db

from suds import TypeNotFound
from suds.client import Client
from suds.xsd.doctor import Import, ImportDoctor
from xml.etree import ElementTree

import random


def process_ws(ws_instance):
    """
        :param ws_instance registry.models.WebService
        return json_response
    """
    if ws_instance.ws_type == 'rest':
        return process_rest(ws_instance)
    elif ws_instance.ws_type == 'wsdl':
        return process_wsdl(ws_instance)
    else:
        raise Exception('There is no web-service type. {}'.format(ws_instance.ws_type))


# ================= REST =====================================
def process_rest(ws_instance):
    # @TO-DO: 1. Check availability
    # send request to erlang server
    ws_instance.status = "up"

    # FOR TESTING only: now popupulate properties with quite random values
    funs = Function.all().ancestor(ws_instance)
    for f in funs:
        f.sensitivity = random.randint(1, 4)
        f.availability = float(int(random.uniform(0.9, 1) * 10000))/10000
        f.mtbf = float(random.randint(500, 10000))
        f.mttr = float(random.randint(1, 5))
        f.reliability = float(int(random.uniform(0.95, 1) * 10000))/10000
        f.uptime = random.randint(1500, 30000)
        f.downtime = random.randint(1500, 30000)
        f.mean_latency = random.randint(2, 200)
        f.std_latency = int(f.mean_latency/random.randint(1, 5))
        f.cost = float(int(random.uniform(0.0001, 0.01) * 10000))/10000
        f.free_day_limit = random.randint(1, 20) * 1000
        f.put()

    ws_instance.put()
    dict_response = ws_instance.to_dict()
    dict_response['functions'] = [db.to_dict(f) for f in funs]
    return json.dumps(dict_response)



# ================= WSDL =====================================
def process_wsdl(ws_instance):
    """
        :param ws_instance registry.models.WebService
        return {"functions": []}, {func_docs: []}, {wsdl_content: ""}
    """
    # Проведем нужные манипуляции
    functions, func_docs, wsdl_content = get_wsdl_data(ws_instance.uri)
    ws_instance.wsdl_content = wsdl_content
    ws_instance.status = 'up'

    # Cохраним веб-сервис
    ws_instance.put()

    # Удалим старые функции веб-сервиса
    # Batch delete
    db.delete(Function.all().ancestor(ws_instance))

    functions_arr = []
    # Сохраним новые функции
    for i, f in enumerate(functions):
        docs = ''
        if len(func_docs) > 0 and func_docs[0] is not None:
            docs = func_docs[i]
        # Каждый веб-сервис имеет набор функций
        # Функции – это отдельные сущности, связанные с ВС
        f2 = Function(name=str(f[0]), arguments=f[1],
                      documentation=docs, parent=ws_instance)
        functions_arr.append(f2)

    # Batch put
    db.put(functions_arr)

    ws_response = db.to_dict(ws_instance, {"id": str(ws_instance.key().id())})
    functions_json = map(db.to_dict, functions_arr)
    ws_response['functions'] = functions_json
    return json.dumps(ws_response)

def get_wsdl_data(wsdl_url):
    """ 
        Get functions as Array 
        and WSDL content as String of WS as a strings
        @TODO: add function's _return_ data
        at the end i need
        ex. getCategoryById(id: int): Category
    """
    # logging.basicConfig(level=logging.INFO)
    # logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)        

    try:
        # Turning off caching in constructor not working
        # So sources of Client have been modified, client.py line 109
        client = Client(wsdl_url, cache=None)
    except TypeNotFound as e:
        logging.warning(e)
        # Ошибка возникает, если WSDL содержит ошибки
        # Тогда пробуем его "вылечить"
        # ============ works for most of services ==============
        imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
        doctor = ImportDoctor(imp)
        try:
            client = Client(wsdl_url, doctor=doctor, cache=None)
        except Exception, e:
            logging.error(e)
            try:
                # ====== works for some of service ===========
                wsdl_content = urllib2.urlopen(wsdl_url)
                
                # Get targetNamespace from root tag
                etElement = ElementTree.fromstring(wsdl_content.read())
                target_namespace = etElement.attrib['targetNamespace']

                imp = Import('http://www.w3.org/2001/XMLSchema')
                imp.filter.add(target_namespace)
                doctor = ImportDoctor(imp)
                client = Client(wsdl_url, doctor=doctor, cache=None)
            except Exception, e:
                raise    
    except Exception, e:
        # Что-то еще случилось, например, такого сервиса вообще не существует
        raise

    functions = []
    for method in client.sd[0].ports[0][1]:
        method_name = method[0]
        #arguments = ''
        arguments = []
        if len(method[1]):
            for arg in method[1]:
                arg_name = arg[1].name
                arg_type = ''
                if arg[1].type == None:
                    try:
                        arg_type = arg[1]._PartElement__resolved.name
                    except Exception:
                        pass
                #arguments += arguments + '{}: {},'.format(arg_name, arg_type)
                arguments.append((arg_name, arg_type))
            #arguments = ', '.join('{}: []'.format(arg.name, part.type[0]) for arg[1] in method[1])
            arguments = ', '.join('{}: {}'. format(a[0], a[1]) for a in arguments)
        else:
            arguments = ''
            # <--- end arguments loop
        functions.append((method_name, arguments))
    # <--- end functions loop

    # Переписать на более универсальный и точный вариант
    etElement = ElementTree.XML(str(client.wsdl.root))
    func_docs = [(node.text) for node in etElement.findall('.//{http://schemas.xmlsoap.org/wsdl/}portType//{http://schemas.xmlsoap.org/wsdl/}documentation')]


    return functions, func_docs, unicode(client.wsdl.root) 
