# coding:utf-8

import webapp2
import json

from google.appengine.ext import db

from registry.models import WebService
from registry.models import WSDLWebService
from registry.models import RESTWebService
from registry.models import Function
from utils.utils import return_json


class ApiHandler(webapp2.RequestHandler):


    @return_json
    def get(self, *args):
        """ Get all functions """
        if self.request.get('format') == 'csv':
            self.get_in_csv()
        else:
            funs = Function.all()
            dict_response = [db.to_dict(f, {"id": str(f.key())}) for f in funs]
            self.response.write(json.dumps(dict_response))


    @return_json
    def delete(self, fun_key):
        f = Function.get(fun_key)
        f.delete()
        self.response.write('ok')


    @return_json
    def put(self, fun_key):
        f = Function.get(fun_key)
        dict_request = json.loads(self.request.body)
        model_properties = f.properties()
        for key, value in dict_request.items():
            if hasattr(f, key):
                model_property = model_properties.get(key)
                if isinstance(model_property, db.FloatProperty):
                    value = float(value)
                elif isinstance(model_property, db.IntegerProperty):
                    value = int(value)
                setattr(f, key, value)

        f.put()
        self.response.write('ok')


    # @return_json
    # def post(self, ws_key):
    #     """ Save function """
    #     dict_request = json.loads(self.request.body)
    #     dict_request['parent'] = db.Key(encoded=ws_key)
    #     function = Function(**dict_request)
    #     function.put()
    #     self.response.write('ok')
